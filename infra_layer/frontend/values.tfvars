/* Project details */
project_name = "coderpolyglot-frontend-prod"
project_id   = "cp-frontend-prod"

/* Region */
region = "us-central1"

/* Enabled apis */
google_service_apis = [
  "cloudresourcemanager.googleapis.com",
  "cloudbilling.googleapis.com",
  "iam.googleapis.com",
  "compute.googleapis.com",
  "serviceusage.googleapis.com",
  "container.googleapis.com",
]
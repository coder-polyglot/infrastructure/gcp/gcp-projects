variable "project_name" {
  type = string
}
variable "project_id" {
  type = string
}
variable "billing_account" {
  type = string
}
variable "org_id" {
  type = number
}
variable "region" {
  type = string
}
variable "terraform_account" {
  type = string
}
variable "terraform_project" {
  type = string
}
variable "google_service_apis" {
  type = list(string)
}
variable "credentials" {
  type    = string
  default = "/Users/ajbarboza/.config/gcloud/admin-barboza-terraform.json"
}
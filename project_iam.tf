resource "google_project_iam_binding" "user_account" {
  project = google_project.project.project_id
  role    = "roles/owner"
  members = ["user:admin-barboza@coderpolyglot.com", "serviceAccount:${data.google_service_account.terraform_account.email}"]
}